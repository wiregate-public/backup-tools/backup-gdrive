# Google Drive to AWS S3 Backup

This repository contains scripts and configurations for backing up files from Google Drive to an AWS S3 bucket using Docker.

## Requirements

- Linux operating system with Docker installed
- An active Google Cloud Platform (GCP) account with appropriate permissions
- An AWS S3 bucket with write access

## Repository Contents

- `config/` - Contains configuration files that are mounted as a volume when the container is run.
  - `.env` - Environment variables needed for the scripts.
  - `rclone.conf` - Configuration for rclone to access Google Drive.
  - `<your_gcp_sa>.json` - JSON file with GCP service account credentials.
- `config_example/` - Contains examples of configuration files.   
- `Dockerfile` - Docker configuration file to build the image.
- `backup_script.py` - Python script that performs the backup.
- `entrypoint.sh` - Script that prepares the environment for backup.

## Setup and Usage

Follow these steps to set up and execute the backup process:

### 1. Clone the Repository

Clone this repository to your local machine:

```bash
git clone <repository-url>
cd <repository-repo-name>
```
### 2. Configure the Environment

Fill in the required variables in the config/ folder. You can refer to the config_example/ directory for examples of how to fill these out.

### 3. Grant Access to Google Drive

Ensure that the GCP service account associated with <your_gcp_sa>.json has Editor (or Creator) permissions on the Google Drive folder that you intend to back up. This is necessary for the service account to be able to read and manage the files within the folder.

### 4. Build the Docker Image

Build the Docker image using the provided Dockerfile:

```
docker build -t restic-gdrive .
```
### 5. Run the Container

Run the container either manually or using a crontab entry:

``` 
docker run -it --rm --privileged -v ./config:/config restic-gdrive
```
Ensure that the container is executed with the necessary privileges and the correct path to the configuration volume is provided.

## Note

This setup assumes that you have set the necessary permissions and have access to both Google Drive and AWS S3. Make sure that the service account used has the required scopes for accessing Google Drive and that the AWS credentials provide sufficient rights to write to the specified S3 bucket.
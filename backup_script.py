import os
import subprocess
import fnmatch

def create_filtered_file_list(local_path, include_patterns, exclude_patterns):
    all_files = []
    for root, dirs, files in os.walk(local_path):
        for file in files:
            file_path = os.path.join(root, file)
            if any(fnmatch.fnmatch(file_path, pattern) for pattern in include_patterns):
                if not any(fnmatch.fnmatch(file_path, pattern) for pattern in exclude_patterns):
                    all_files.append(file_path)
    
    file_list_path = '/tmp/restic_file_list.txt'
    with open(file_list_path, 'w') as file_list:
        for file in all_files:
            file_list.write(file + '\n')
    
    return file_list_path

def run_restic_backup(file_list_path, restic_repo, restic_password, aws_access_id, aws_secret_key):
    env = os.environ.copy()
    env["RESTIC_REPOSITORY"] = restic_repo
    env["RESTIC_PASSWORD"] = restic_password
    env["AWS_ACCESS_KEY_ID"] = aws_access_id
    env["AWS_SECRET_ACCESS_KEY"] = aws_secret_key

    # Perform restic backup to S3
    backup_command = [
    'restic', '-r', restic_repo, 'backup',
    '--files-from', file_list_path,
    '--verbose=3', '--ignore-inode' ]
    #backup_command = ["restic", "backup", "--files-from", file_list_path, "--verbose"]
    subprocess.run(backup_command, env=env, check=True)

    # Clean old backups according to a retention policy
    subprocess.run(["restic", "forget", "--keep-daily", "7", "--keep-weekly", "4", "--keep-monthly", "6", "--prune"], env=env, check=True)

def main():
    local_path = '/mnt/google_drive'
    restic_repo = os.getenv('RESTIC_REPOSITORY')
    restic_password = os.getenv('RESTIC_PASSWORD')
    aws_access_id = os.getenv('AWS_ACCESS_KEY_ID')
    aws_secret_key = os.getenv('AWS_SECRET_ACCESS_KEY')
    include_patterns = os.getenv('FILE_PATTERNS', '*').split(',')
    exclude_patterns = os.getenv('IGNORE_PATTERNS', '').split(',')

    # Ensure all needed environment variables are set
    if not all([restic_repo, restic_password, aws_access_id, aws_secret_key]):
        raise EnvironmentError("One or more environment variables required for Restic are not set")

    file_list_path = create_filtered_file_list(local_path, include_patterns, exclude_patterns)
    run_restic_backup(file_list_path, restic_repo, restic_password, aws_access_id, aws_secret_key)
    os.remove(file_list_path)  # Cleanup the temporary file list after backup

if __name__ == "__main__":
    main()


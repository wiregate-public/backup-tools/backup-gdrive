# Use Ubuntu as the base image
FROM ubuntu:latest

# Update package lists and install necessary software
RUN apt-get update && \
    apt-get install -y curl wget unzip fuse3 python3 python3-pip && \
    apt-get clean && \
    rm -rf /var/lib/apt/lists/*

RUN wget https://github.com/restic/restic/releases/download/v0.16.4/restic_0.16.4_linux_amd64.bz2
#RUN restic self-update    

RUN bzip2 -d restic_0.16.4_linux_amd64.bz2 && \
    chmod +x restic_0.16.4_linux_amd64 && \
    mv restic_0.16.4_linux_amd64 /usr/local/bin/restic

RUN curl https://rclone.org/install.sh | bash

# Copy scripts into the container
COPY backup_script.py /app/backup_script.py
COPY entrypoint.sh /app/entrypoint.sh

# Make scripts executable
RUN chmod +x /app/backup_script.py /app/entrypoint.sh
#ENV GOOGLE_APPLICATION_CREDENTIALS=/config/admin-sa.json

# Set the entrypoint to run your script
ENTRYPOINT ["/app/entrypoint.sh"]
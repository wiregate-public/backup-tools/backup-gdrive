#!/bin/bash

# Load environment variables from .env file
if [ -f /config/.env ]; then
    set -o allexport
    echo "Sourcing environment from .env"
    source /config/.env
    set +o allexport
else
    echo "Error: .env file not found."
    exit 1
fi

# Ensure rclone configuration file is present
if [ ! -f /config/rclone.conf ]; then
    echo "Error: rclone configuration file not found."
    exit 1
else
    mkdir -p /root/.config/rclone
    cp /config/rclone.conf /root/.config/rclone/rclone.conf    
fi

# Export the path to the rclone configuration file
export RCLONE_CONFIG=/config/rclone.conf

# Check required AWS credentials
if [ -z "$AWS_ACCESS_KEY_ID" ] || [ -z "$AWS_SECRET_ACCESS_KEY" ]; then
    echo "AWS credentials are not set."
    exit 1
fi

# Check if the restic repository and password are set
if [ -z "$RESTIC_REPOSITORY" ] || [ -z "$RESTIC_PASSWORD" ]; then
    echo "Restic repository or password not set."
    exit 1
fi

# Check file patterns, ensuring they are not empty. Use defaults if necessary.
if [ -z "$FILE_PATTERNS" ]; then
    export FILE_PATTERNS="*"
    echo "Warning: FILE_PATTERNS is not set. Defaulting to '*' to include all files."
fi

if [ -z "$IGNORE_PATTERNS" ]; then
    export IGNORE_PATTERNS=""
    echo "Warning: IGNORE_PATTERNS is not set. No files will be ignored."
fi

# Mount Google Drive
mkdir -p /mnt/google_drive

#rclone mount google_drive: /mnt/google_drive &
rclone mount google_drive: /mnt/google_drive --daemon --allow-other --read-only --vfs-cache-mode full
sleep 5 # it takes some time

# Check if mounted successfully
if mountpoint -q /mnt/google_drive; then
    echo "Google Drive mounted successfully."
else
    echo "Failed to mount Google Drive."
    exit 1
fi

# mkdir /mnt/lower /mnt/upper /mnt/work /mnt/merged
# mount -t overlay overlay -o lowerdir=/mnt/google_drive,upperdir=/mnt/upper,workdir=/mnt/work /mnt/merged

## Solution with copying all files to the local dir
# mkdir -p /local/temp_backup
# rsync -av /mnt/google_drive/ /local/temp_backup/


if ! restic -r $RESTIC_REPOSITORY snapshots > /dev/null 2>&1; then
    echo "Init S3 repository ..."
    restic -r $RESTIC_REPOSITORY init
fi

# Run the Python backup script
python3 /app/backup_script.py


# Unmount Google Drive after the backup
fusermount -u /mnt/google_drive